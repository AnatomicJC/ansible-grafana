#!/bin/sh

SCRIPT_DIR=$(cd -- "$(dirname -- "$0")" && pwd -P)

git clone git@github.com:confluentinc/jmx-monitoring-stacks.git

rm ${SCRIPT_DIR}/../files/var/lib/grafana/dashboards/*json
cp ${SCRIPT_DIR}/jmx-monitoring-stacks/jmxexporter-prometheus-grafana/assets/grafana/provisioning/dashboards/*.json \
   ${SCRIPT_DIR}/../files/var/lib/grafana/dashboards/

rm -rf jmx-monitoring-stacks